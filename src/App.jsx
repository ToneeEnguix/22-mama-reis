import React from 'react'
import {
  Route,
  BrowserRouter as Router,
  Routes,
  Navigate
} from 'react-router-dom'
import Home from './pages/Home.jsx'
import Grito from './pages/Grito.jsx'
import Orfa from './pages/Orfa.jsx'
import Love from './pages/Love.jsx'
import Orfa2 from './pages/Orfa2.jsx'
import Final from './pages/Final.jsx'

export default function App () {
  return (
    <Router>
      <Routes>
        <Route exact path='/' element={<Home />} />
        <Route exact path='/grito' element={<Grito />} />
        <Route exact path='/orfa' element={<Orfa />} />
        <Route exact path='/orfa2' element={<Orfa2 />} />
        <Route exact path='/final' element={<Final />} />
        <Route exact path='/love' element={<Love />} />
        <Route path='/*' component={<Navigate to='/' />} />
      </Routes>
    </Router>
  )
}
