import React from 'react'
import { useNavigate } from 'react-router-dom'
import arrow from '../assets/back.gif'

export default function Back () {
  const navigate = useNavigate()

  return (
    <div onClick={() => navigate(-1)} className='backStyle pointer'>
      <img alt='arrow' src={arrow} className='pointer' />
    </div>
  )
}
