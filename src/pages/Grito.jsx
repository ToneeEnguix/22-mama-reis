import React from 'react'
import { Link } from 'react-router-dom'
import Back from '../comps/Back'

export default function Grito () {
  return (
    <div className='main grito flexColumn'>
      <Back />
      <div>
        <h3>De los creadores de:</h3>
        <h3 className='rainbow'>EL GRITO</h3>
        <h3>llega...</h3>
        <Link to='/orfa'>Continua</Link>
      </div>
    </div>
  )
}
