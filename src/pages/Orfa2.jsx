import React from 'react'
import { Link } from 'react-router-dom'
import Back from '../comps/Back'

export default function Orfa2 () {
  return (
    <div className='main orfa2 flexColumn'>
      <Back />
      <div>
        <h5 style={{ maxWidth: '70vw', lineHeight: '100px' }}>
          Val per una tarda "de por" amb els teus mimosos al Escape Room
          Abduction de Badalona
        </h5>
        <Link to='/love'>Continua</Link>
      </div>
    </div>
  )
}
