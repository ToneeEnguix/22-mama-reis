import React from 'react'
import CountUp from 'react-countup'
import { Link } from 'react-router-dom'

export default function Home () {
  return (
    <div className='main home flexColumn'>
      <h1>HEY MOM!</h1>
      <div>
        Feliços Reis <CountUp end={2023} />
      </div>
      <Link to='/grito'>Continua</Link>
    </div>
  )
}
