import React from 'react'
import { Link } from 'react-router-dom'
import Back from '../comps/Back'

export default function Love () {
  return (
    <div className='main love flexColumn'>
      <Back />
      <div>
        <Link to='/final'>
          <h1>we love you mom</h1>
        </Link>
      </div>
    </div>
  )
}
