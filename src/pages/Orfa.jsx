import React from 'react'
import { Link } from 'react-router-dom'
import Back from '../comps/Back'

export default function Orfa () {
  return (
    <div className='main orfa flexColumn'>
      <Back />
      <div>
        <h1>- EL ORFANATO -</h1>
        <Link to='/orfa2'>Continua</Link>
      </div>
    </div>
  )
}
